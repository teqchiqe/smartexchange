import { Component, OnInit } from '@angular/core';
import { PostComponent } from '../post/post.component';

@Component({
  selector: 'app-transfer-details',
  templateUrl: './transfer-details.component.html',
  styleUrls: ['./transfer-details.component.css']
})
export class TransferDetailsComponent implements OnInit {
  data;
  srcCurrency = '0';
  destCurrency = '1';
  srcVal = 10000;
  destVal;
  map = ['NOK', 'INR','USD','EUR','PKR'];
  exRate = 8.29;
  constructor() { }

  ngOnInit() {
    this.data = window.appData;
    this.destVal = (this.srcVal * this.exRate).toFixed(2);
  }

  showStatus() {
    document.getElementById('mat-tab-label-0-2').click();
  }

}
