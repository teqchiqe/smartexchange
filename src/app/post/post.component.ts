import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  srcCurrency = '0';
  destCurrency = '1';
  srcVal = 10000;
  destVal;
  map = ['NOK', 'INR','USD','EUR','PKR'];
  exRate;
  @Input() data;
  adList: any = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    setTimeout(() => {
      this.changed();
    }, 1000);
    this.http.get(window.api + 'advertisement').subscribe(data => {
      console.log(data);
      this.adList = data;
    });
  }

  changed() {
    console.log('from-',this.srcVal, ' to ', this.destVal);
    let dest = window.fx.convert(this.srcVal, {from: this.map[parseInt(this.srcCurrency)], to: this.map[parseInt(this.destCurrency)]})
    this.exRate = window.fx.convert(1, {from: this.map[parseInt(this.srcCurrency)], to: this.map[parseInt(this.destCurrency)]});
    this.exRate = this.exRate.toFixed(2);
    console.log(dest);
    this.destVal = dest.toFixed(2);
  }

  start() {
    window.appData = {
      srcVal : this.srcVal,
      destCurrency : this.destCurrency,
      srcCurrency : this.srcCurrency
    };
    this.adList.push({
      name : 'Tom',
      srcValue:  this.srcVal,
      minExRate: this.exRate,
      srcCurrency: this.map[parseInt(this.srcCurrency)],
      destCurrency: this.map[parseInt(this.destCurrency)],
      status: 0
   });

    document.getElementById('mat-tab-label-0-1').click();
  }

  deal() {
    document.getElementById('mat-tab-label-0-1').click();
  }

}
