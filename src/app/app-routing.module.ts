import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent }      from './post/post.component';
import { TransferDetailsComponent }      from './transfer-details/transfer-details.component';
import { StatusComponent }      from './status/status.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: 'post', component: PostComponent},
  { path: 'transfer-details', component : TransferDetailsComponent },
  { path: 'status', component : StatusComponent }
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule ],
  declarations: []
})
export class AppRoutingModule { 

}
